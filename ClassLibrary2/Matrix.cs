﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public delegate  double MatrixElement(int n, int j, bool row);
    public class Matrix
    {
         public int Size { get; set; }
        public DateTime Time { get; set; }
        public MatrixElement SetMatrix{get; set;}

        public double[] x = null;
        public double[] y = null;
        private double[] new_x;
        private double[] new_y;
        double[] previous_y;
        double[] previous_x;






        public Matrix(int size, MatrixElement elem) {

            Size = size;
            x = new double[Size];
            y = new double[Size];
            new_x = new double[Size];
            new_y = new double[Size];

            
            
            
            for (int i = 0; i<size; i++)
            {
                x[i] = elem(Size, i, true);
                y[i] = elem(Size, i, false);


             }
        }
         
        public override string ToString()
        {
            string res = "";
            for (int i = 0; i< Size; i++)
            {
                for (int j = 0; j<Size; j++)
                {
                    if (j >= i)
                    {
                        res += x[j - i] + " ";
                    }
                    else
                    {
                        res += y[i - j] + " ";
                    }
                }
                res += "\n";
            }
            return res;
        }

        private double calculate_Fk(int k)
        {
            double Fk = 0;
            for (int i = 0; i<k; i++)
            {
                Fk += x[k - i] * new_x[i]; 
            }
            return Fk;
        }

        private double calculate_Gk(int k)
        {
            double Gk = 0;
            for (int i = 0; i < k; i++)
            {
                Gk += y[i+1] * new_y[i];
            }
            return Gk;
        }

        private void calculate_new_list_item(int k, double Rk, double Sk, double Tk)
        {
            previous_x = new_x;
            previous_y = new_y;


            for (int i = k; i > 0; i--)
            {
                previous_y[i] = previous_y[i - 1];
            }
            previous_y[0] = 0;

            new_x = new double[Size];
            new_y = new double[Size];


            for (int i = 0; i <= k; i++)
            {
                new_x[i] = previous_x[i] * Rk + previous_y[i] * Sk;
                new_y[i] = previous_x[i] * Tk + previous_y[i] * Rk;
            }




        }

        
        private void multiply_matrixes(double[] rh, double[] res)
        {
            double[] temp1 = new double[Size];
            double[] temp2 = new double[Size];
          

            for (int i = 0; i < Size; i++)
            {
                for (int j = i; j < Size; j++)
                {
                    temp1[i] += new_y[j] * rh[Size - (j - i) - 1];
                }
                for (int j = i; j >=0; j--)
                {
                    res[i] += new_x[j] * temp1[i-j];
                }
                for (int j = i+1; j < Size ; j++)
                {
                    temp2[i] += new_x[j] * rh[Size - (j - i - 1) - 1];
                }
                for (int j = i - 1; j >= 0; j--)
                {
                    res[i] = res[i] - new_y[j] * temp2[i - j - 1];
                }
                res[i] = res[i] / new_x[0];
            }
        }
                    




        public bool Solve(double[] rh, double[] res)
        {

            new_x[0] = 1 / x[0];
            new_y[0] = 1 / y[0];

            double Fk, Gk, Rk, Sk, Tk;
            for (int k = 1; k<Size; k++)
            {
                Fk = calculate_Fk(k);
                Gk = calculate_Gk(k);
                Rk = 1 / (1 - Fk * Gk);
                Sk = -(Rk * Fk);
                Tk = -(Rk * Gk);
                calculate_new_list_item(k, Rk, Sk, Tk);

            }

            multiply_matrixes(rh, res);
            return true;
        }

        public double this[int r, int c]
        {
            get
            {
                if ( r > c)
                {
                    return (y[r - c]);
                }
                else
                {
                    return (x[c - r]);
                }
                    
            }
            set { }
            
        }
    }
}








