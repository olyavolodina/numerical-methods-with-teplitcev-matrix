﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ConsoleApp1
{
    [Serializable]
    public class TestTime
    {
        private List<TestTimeItem> Lst;
        public void Add(TestTimeItem item)
        {
            if (Lst == null)
            {
                Lst = new List<TestTimeItem>();
            }
             Lst.Add(item);
        }
        
        public static bool Save(string filename, TestTime obj)
        {
                FileStream filestream = null;
            try
            {
                filestream = File.Create(filename);
                BinaryFormatter binaryformatter = new BinaryFormatter();

                binaryformatter.Serialize(filestream, obj);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            finally
            {
                if (filestream != null)
                {
                    filestream.Close();
                }
            }
        }

        public static bool Load(string filename, ref TestTime obj)
        {
            FileStream filestream = null;
            try
            {
                filestream = File.OpenRead(filename);
                BinaryFormatter binaryformatter = new BinaryFormatter();
                obj = binaryformatter.Deserialize(filestream) as TestTime;
                return true;


            }
            catch (FileNotFoundException )
            {
                return false;
                
            }
            catch ( Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
            finally
            {
                if (filestream != null)
                {
                    filestream.Close();
                }
                
            }
        }

        public override string ToString()
        {
            string res = "";
            foreach (var item in Lst)
            {
                res += item.ToString() + " " ;
            }
            return res;
        }




    }
}
