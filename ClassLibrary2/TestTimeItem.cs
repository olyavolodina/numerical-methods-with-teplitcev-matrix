﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using System.Diagnostics;
using System.Runtime.InteropServices;

namespace ConsoleApp1
{


    [Serializable]
     public class TestTimeItem
    {
        //[DllImport(@"C:\Users\Оля\source\repos\num_method\Debug\num_method.dll")]
        [DllImport(@"num_method.dll")]
        public static extern void global_func(int size, double[] row, double[] column, double[] rh, double[] res, ref double time);
        public int Size { get; set; }
        public double C_sharp_time { get; set; }
        public double C_plus_time { get; set; }
        public double Coef { get; private set; }
        
        
        public override string ToString()
        {
            return ("\nSize: " + Size + " C#: " + C_sharp_time.ToString() + " C++: " + C_plus_time.ToString() + " Coefficient: " + Coef.ToString());
        }


          
        public static double MatElem(int n, int j, bool row)
        {
            if (j == 0)
            {
                return n * n;
            }
            if (row == true)
                return (n - j);
            else
                return (n - j + 1);
        }

        public static void Test_C_plus(int size, double[] x, double[]y, double[]rh,  ref double[] res_plus)
        {
            double time = 0;
            global_func(size, x, y, rh, res_plus, ref time);
            
        }

        

        public void Measure(int size)
        {
            Size = size;
            MatrixElement me = new MatrixElement(MatElem);
            double[] rh = new double[Size];
            for (int i = 0; i < Size; i++)
            {
                rh[i] = i + 1;
            }
            double[] res_sharp = new double[Size];
            double[] res_plus = new double[Size];

         
            Matrix M1 = new Matrix(Size, me);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            M1.Solve(rh, res_sharp);
            watch.Stop();
            C_sharp_time = (int)watch.Elapsed.TotalMilliseconds;
            //C_sharp_time = watch.Elapsed.TotalMilliseconds;



            double time = 0;
            double[] x = new double[Size];
            double[] y = new double[Size];
            for (int i = 0; i < Size; i++)
            {
                x[i] = MatElem(Size, i, true);
                y[i] = MatElem(Size, i, false);

            }
            global_func(Size, x, y, rh, res_plus, ref time);


            C_plus_time = time;
           
            Coef = C_sharp_time / C_plus_time;

            
        }
    }
}
