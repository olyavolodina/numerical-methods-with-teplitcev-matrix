﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {

        public static void Test()
        {
            
            int size = 5;
            MatrixElement me = TestTimeItem.MatElem;
            Matrix M = new Matrix(size, me);

            double[] rh = new double[size];
            for (int i = 0; i < size; i++)
            {
                rh[i] = i + 1;
            }

            double[] res_plus = new double[size];
            double[] res_sharp = new double[size];
            double[] rh_check = new double[size];

            M.Solve(rh, res_sharp); // решаем на  С#
            TestTimeItem.Test_C_plus(size, M.x, M.y, rh,  ref res_plus);  // решаем на С++

            Console.WriteLine("Заданная матрица");
            Console.WriteLine(M.ToString());
            Console.WriteLine("Правая часть");
            foreach (var item in rh)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine("\n\nРезультат на С++" );
            foreach (var item in res_plus)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine("\nРезультат на С#");
            foreach (var item in res_sharp)
            {
                Console.Write(item + " ");
            }


            for (int i = 0; i<size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rh_check[i] += res_plus[j] * M[j, i]; 

                }

            }
            Console.WriteLine("\n\nУмножаем матрицу на полученный результат");
            foreach (var item in rh_check)
            {
                Console.Write(item + " ");
            }
            


            

        }
        static void Main(string[] args)
        {

            Test();
            TestTime T = new TestTime();
            string filename = "loaded.txt";
            TestTime.Load(filename, ref T);

            try
            {
                while (true)
                {
                    Console.WriteLine("\nPress 'e' to exit or enter matrix size");
                    int order;
                    string input = Console.ReadLine();
                    if (input == "e")
                    {
                        break;
                    }
                    if (Int32.TryParse(input, out order) == false)
                    {
                        continue;
                    }
                    if (order <= 0 )
                    {
                        continue;
                    }

                    TestTimeItem t = new TestTimeItem();
                    t.Measure(order);
                    T.Add(t);
                    Console.WriteLine(t.ToString());
                }

                Console.WriteLine("\nВсе полученные данные");
                Console.WriteLine(T.ToString());
                TestTime.Save(filename, T);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }





        }
    }
}
