// num_method.cpp : Defines the exported functions for the DLL application.
//


#include "stdafx.h"
#include <chrono>
using namespace std;
class Matrix {
private:
	int size;
	double* x;
	double* y;
	double* new_x;
	double* new_y;
	double* previous_x;
	double* previous_y;
	int time;





	double calculate_Fk(int k)
	{
		double Fk = 0;
		for (int i = 0; i < k; i++)
		{
			Fk += x[k - i] * new_x[i];
		}
		return Fk;
	}


	double calculate_Gk(int k)
	{
		double Gk = 0;
		for (int i = 0; i < k; i++)
		{
			Gk += y[i + 1] * new_y[i];
		}
		return Gk;
	}

	void multiply_matrixes(double* rh, double* res)
	{
		double* temp1 = new double[size]();
		double* temp2 = new double[size]();
		double* temp3 = new double[size]();
		double* temp4 = new double[size]();

		for (int i = 0; i < size; i++)
		{
			for (int j = i; j < size; j++)
			{
				temp1[i] += new_y[j] * rh[size - (j - i) - 1];
			}
			for (int j = i; j >= 0; j--)
			{
				temp2[i] += new_x[j] * temp1[i - j];
			}

			for (int j = i + 1; j < size; j++)
			{
				temp3[i] += new_x[j] * rh[size - (j - i - 1) - 1];
			}

			for (int j = i - 1; j >= 0; j--)
			{
				temp4[i] += new_y[j] * temp3[i - j - 1];
			}
		}

		for (int i = 0; i < size; i++)
		{
			res[i] = (temp2[i] - temp4[i]) / new_x[0];

		}

		delete[] temp1;
		delete[] temp2;
		delete[] temp3;
		delete[] temp4;



	}
	

	void calculate_new_list_item(int k, double Rk, double Sk, double Tk)
	{
		double*  previous_x = new_x;
		double* previous_y = new_y;


		for (int i = k; i > 0; i--) {
			previous_y[i] = new_y[i - 1];
		}
		previous_y[0] = 0;

		new_x = new double[size]();
		new_y = new double[size]();


		for (int i = 0; i <= k; i++)
		{
			new_x[i] = previous_x[i] * Rk + previous_y[i] * Sk;
			new_y[i] = previous_x[i] * Tk + previous_y[i] * Rk;
		}

		delete[] previous_x;
		delete[] previous_y;
	}
public:
	~Matrix() {
		delete[] x;
		delete[] y;
	}

	Matrix(int s, double* r, double* c) {
		size = s;
		x = new double[size];
		y = new double[size];

		for (int i = 0; i < s; i++) {
			x[i] = r[i];
			y[i] = c[i];
		}
	}

	bool Solve(double* rh, double* res)
	{



		new_x = new double[size]();
		new_y = new double[size]();
		new_x[0] = 1 / x[0];
		new_y[0] = 1 / y[0];


		double Fk, Gk, Rk, Sk, Tk;
		for (int k = 1; k < size; k++)
		{
			Fk = calculate_Fk(k);
			Gk = calculate_Gk(k);
			Rk = 1 / (1 - Fk * Gk);
			Sk = -(Rk * Fk);
			Tk = -(Rk * Gk);
			calculate_new_list_item(k, Rk, Sk, Tk);

		}

		multiply_matrixes(rh, res);
		delete[] new_x;
		delete[] new_y;
		return true;
	}



};

extern "C" {
	__declspec(dllexport) void __stdcall global_func(int size, double* row, double* column, double* rh, double* res, double* time);
}


  __declspec(dllexport)
void __stdcall global_func(int size, double* row, double* column, double* rh, double* res, double* time) {
	Matrix M(size, row, column);
	auto start = chrono::steady_clock::now();
	M.Solve(rh, res);
	auto end = chrono::steady_clock::now();
	*time = chrono::duration_cast<chrono::milliseconds>(end - start).count();

}
